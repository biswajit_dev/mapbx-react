export const collegeData = [
  {
    id: 1,
    name: "Techno India University",
    geomatry: [22.5759, 88.4283],
  },
  {
    id: 2,
    name: "Techno International New Town",
    geomatry: [22.5784, 88.4758],
  },
  {
    id: 3,
    name: "Netaji Subhash Engineering College",
    geomatry: [22.4762, 88.4149],
  },
  {
    id: 4,
    name: "Bengal Institute of Technology, Kolkata",
    geomatry: [22.5216, 88.4609],
  },
  {
    id: 5,
    name: "Institute of Engineering and Management",
    geomatry: [22.5744, 88.4338],
  },
  {
    id: 6,
    name: "Techno India Batanagar",
    geomatry: [22.4922, 88.2227],
  },
  {
    id: 7,
    name: "Heritage Institute of Technology, Kolkata",
    geomatry: [22.5165, 88.4175],
  },
  {
    id: 8,
    name: "Haldia Institute Of Technology",
    geomatry: [22.0506, 88.0722],
  },
  {
    id: 9,
    name: "Abacus Institute of Engineering and Management",
    geomatry: [22.9921, 88.3643],
  },
  {
    id: 10,
    name: "Narula Institute of Technology",
    geomatry: [22.6767, 88.3791],
  },
  {
    id: 11,
    name: "University of Engineering & Management (UEM), Kolkata",
    geomatry: [22.5602, 88.4902],
  },
  {
    id: 12,
    name: "The Neotia University",
    geomatry: [22.2608, 88.1979],
  },
  {
    id: 13,
    name: "Amity University Kolkata",
    geomatry: [22.5958, 88.4877],
  },
];
