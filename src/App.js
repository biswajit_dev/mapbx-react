import React, { useState } from "react";
import ReactMapGL, { Marker } from "react-map-gl";
import { collegeData } from "./collegeData";
import "./main.css";

const App = () => {
  const [viewPort, setViewPort] = useState({
    latitude: 22.5726,
    longitude: 88.3639,
    zoom: 8,
  });

  return (
    <ReactMapGL
      {...viewPort}
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_AUTH_KEY}
      width="100vw"
      height="100vh"
      mapStyle="mapbox://styles/capitalbiswajit99/ckisznfzf1g2t19nys2v43ugy"
      onViewportChange={(viewport) => setViewPort(viewport)}
    >
      {collegeData.map((college) => (
        <Marker
          key={college.id}
          latitude={college.geomatry[0]}
          longitude={college.geomatry[1]}
        >
          <div className="main-class">
            <p className="sub-class">{college.name}</p>
          </div>
        </Marker>
      ))}
    </ReactMapGL>
  );
};

export default App;
